package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class WeatherLogHeaderResponse {

    @JsonProperty(value = "cnt")
    private int count;

    @JsonProperty(value = "list")
    private List<WeatherLogResponse> weatherLogResponseList;
}
