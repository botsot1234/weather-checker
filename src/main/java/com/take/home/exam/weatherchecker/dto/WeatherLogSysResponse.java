package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherLogSysResponse {

    @JsonProperty(value = "type")
    private int type;

    @JsonProperty(value = "id")
    private long id;

    @JsonProperty(value = "message")
    private double message;

    @JsonProperty(value = "country")
    private String country;

    @JsonProperty(value = "sunrise")
    private long sunrise;

    @JsonProperty(value = "sunset")
    private long sunset;
}
