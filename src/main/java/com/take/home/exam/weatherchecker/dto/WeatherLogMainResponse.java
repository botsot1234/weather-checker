package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherLogMainResponse {

    @JsonProperty(value = "temp")
    private double temp;

    @JsonProperty(value = "pressure")
    private double pressure;

    @JsonProperty(value = "humidity")
    private double humidity;

    @JsonProperty(value = "temp_main")
    private double tempMain;

    @JsonProperty(value = "temp_max")
    private double tempMax;
}
