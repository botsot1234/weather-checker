package com.take.home.exam.weatherchecker.repository;

import com.take.home.exam.weatherchecker.model.WeatherLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherLogRepo extends JpaRepository<WeatherLog, Long> {

}
