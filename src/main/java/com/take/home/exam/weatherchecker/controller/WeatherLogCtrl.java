package com.take.home.exam.weatherchecker.controller;

import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoListResponse;
import com.take.home.exam.weatherchecker.service.WeatherLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherLogCtrl {

    @Autowired
    private WeatherLogService weatherLogService;


    @GetMapping(value = "/check", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WeatherLogShortInfoListResponse> check() {
        WeatherLogShortInfoListResponse response = weatherLogService.getWeatherData();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping(value = "/store", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WeatherLogShortInfoListResponse> save() {
        WeatherLogShortInfoListResponse response = weatherLogService.save();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
