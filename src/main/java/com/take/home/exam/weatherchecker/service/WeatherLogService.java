package com.take.home.exam.weatherchecker.service;

import com.take.home.exam.weatherchecker.dto.WeatherLogDetailResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogHeaderResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoListResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoResponse;
import com.take.home.exam.weatherchecker.model.WeatherLog;
import com.take.home.exam.weatherchecker.repository.WeatherLogRepo;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class WeatherLogService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    WeatherLogRepo weatherLogRepo;

    // api key - aea6b8475854db3d317d1948f253dfbb
    // 5391959 - San Francisco, US
    // 2643743 - London, UK,
    // 3067696 - Prague, CZ
    // http://api.openweathermap.org/data/2.5/group?id=5391959,2643743,3067696&units=metric&appid=aea6b8475854db3d317d1948f253dfbb

    private static final String URI = "http://api.openweathermap.org/data/2.5/group?id=5391959,2643743,3067696&units=metric&appid=aea6b8475854db3d317d1948f253dfbb";
    private static final String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";


    public WeatherLogShortInfoListResponse getWeatherData() {
        List<WeatherLogResponse> weatherLogResponseList = getWeatherLogResponseList();

        WeatherLogShortInfoListResponse weatherLogShortInfoListResponse = new WeatherLogShortInfoListResponse();
        List<WeatherLogShortInfoResponse> infoResponses = new ArrayList<>();

        buildAndSave(infoResponses, false, weatherLogResponseList);

        weatherLogShortInfoListResponse.setList(infoResponses);

        return weatherLogShortInfoListResponse;
    }

    private List<WeatherLogResponse> getWeatherLogResponseList() {
        ResponseEntity<WeatherLogHeaderResponse> response = restTemplate
            .getForEntity(URI, WeatherLogHeaderResponse.class);
        WeatherLogHeaderResponse apiResult = response.getBody();
        log.info("Body: {}", apiResult);
        return apiResult.getWeatherLogResponseList();
    }

    public WeatherLogShortInfoListResponse save() {
        WeatherLogShortInfoListResponse weatherLogShortInfoListResponse = new WeatherLogShortInfoListResponse();

        List<WeatherLogResponse> weatherLogResponseList = getWeatherLogResponseList();
        List<WeatherLogShortInfoResponse> infoResponses = new ArrayList<>();

        buildAndSave(infoResponses, true, weatherLogResponseList);
        weatherLogShortInfoListResponse.setList(infoResponses);

        return weatherLogShortInfoListResponse;
    }

    private void buildAndSave(List<WeatherLogShortInfoResponse> infoResponses, boolean isSave,
        List<WeatherLogResponse> weatherLogResponseList) {
        if (!ObjectUtils.isEmpty(weatherLogResponseList)) {
            weatherLogResponseList.forEach(weatherLogResponse -> {
                WeatherLog weatherLog = build(weatherLogResponse);
                infoResponses.add(new WeatherLogShortInfoResponse(weatherLog.getLocation(),
                    weatherLog.getActualWeather(),
                    weatherLog.getTemperature()));
                if (isSave) {
                    weatherLogRepo.save(weatherLog);
                }
            });
        }
    }

    private String generateResponseId() {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private WeatherLog build(WeatherLogResponse weatherLogResponse) {
        WeatherLog weatherLog = new WeatherLog();
        weatherLog.setResponseId(generateResponseId());
        String location = getLocation(weatherLogResponse);
        List<WeatherLogDetailResponse> weatherLogDetailResponses = weatherLogResponse
            .getWeatherLogDetailResponseList();

        checkActualWeather(weatherLog, weatherLogDetailResponses);

        String temperature =
            String.valueOf(weatherLogResponse.getWeatherLogMainResponse().getTemp())
                + " Degrees Celcius";
        weatherLog.setTemperature(temperature);
        weatherLog.setLocation(location);

        log.info("WeatherLog: {}", weatherLog);

        return weatherLog;
    }

    private String getLocation(WeatherLogResponse weatherLogResponse) {
        return weatherLogResponse.getName() + ", " + weatherLogResponse.getWeatherLogSysResponse()
            .getCountry();
    }

    private void checkActualWeather(WeatherLog weatherLog,
        List<WeatherLogDetailResponse> weatherLogDetailResponses) {
        if (!ObjectUtils.isEmpty(weatherLogDetailResponses)) {
            String actualWeatherJoined = String.join(", ", weatherLogDetailResponses.stream()
                .map(weatherLogDetailResponse -> weatherLogDetailResponse.getDescription())
                .collect(
                    Collectors.toList()));
            weatherLog.setActualWeather(actualWeatherJoined);
        }
    }
}
