package com.take.home.exam.weatherchecker.dto;

import java.util.List;
import lombok.Data;

@Data
public class WeatherLogShortInfoListResponse {

    private List<WeatherLogShortInfoResponse> list;
}
