package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherLogDetailResponse {

    @JsonProperty(value = "id")
    private long id;

    @JsonProperty(value = "main")
    private String main;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "icon")
    private String icon;
}
