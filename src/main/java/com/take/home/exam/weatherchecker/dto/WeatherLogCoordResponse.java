package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherLogCoordResponse {

    @JsonProperty("lon")
    private double lon;

    @JsonProperty("lan")
    private double lan;
}
