package com.take.home.exam.weatherchecker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class WeatherLogResponse {

    @JsonProperty(value = "coord")
    private WeatherLogCoordResponse weatherLogCoordResponse;

    @JsonProperty(value = "sys")
    private WeatherLogSysResponse weatherLogSysResponse;

    @JsonProperty(value = "weather")
    private List<WeatherLogDetailResponse> weatherLogDetailResponseList;

    @JsonProperty(value = "main")
    private WeatherLogMainResponse weatherLogMainResponse;

    private double visibility;

    @JsonProperty("wind")
    private WeatherLogWindResponse weatherLogWindResponse;

    @JsonProperty("clouds")
    private WeatherLogCloudsResponse weatherLogCloudsResponse;

    @JsonProperty("dt")
    private long dateTimeStamp;

    private long id;

    private String name;
}
