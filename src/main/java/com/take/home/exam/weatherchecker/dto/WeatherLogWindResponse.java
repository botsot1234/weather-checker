package com.take.home.exam.weatherchecker.dto;

import lombok.Data;

@Data
public class WeatherLogWindResponse {

    private double speed;
    private double deg;
}
