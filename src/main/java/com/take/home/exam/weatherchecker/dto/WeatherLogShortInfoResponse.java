package com.take.home.exam.weatherchecker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WeatherLogShortInfoResponse {

    private String location;
    private String actualWeather;
    private String temperature;
}
