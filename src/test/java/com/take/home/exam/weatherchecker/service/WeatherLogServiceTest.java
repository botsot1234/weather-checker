package com.take.home.exam.weatherchecker.service;

import com.take.home.exam.weatherchecker.dto.WeatherLogDetailResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogHeaderResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogMainResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoListResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogSysResponse;
import com.take.home.exam.weatherchecker.model.WeatherLog;
import com.take.home.exam.weatherchecker.repository.WeatherLogRepo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(PowerMockRunner.class)
public class WeatherLogServiceTest {

    @InjectMocks
    private WeatherLogService sut;

    @Mock
    private WeatherLogRepo weatherLogRepo;

    @Mock
    private RestTemplate restTemplate;


    @Before
    public void setUp() {
        WeatherLogHeaderResponse weatherLogHeaderResponse = new WeatherLogHeaderResponse();
        ResponseEntity<WeatherLogHeaderResponse> responseResponseEntity = new ResponseEntity<>(
            weatherLogHeaderResponse, HttpStatus.OK);
        Mockito.when(restTemplate.getForEntity(Mockito.anyString(), Mockito.any(Class.class)))
            .thenReturn(responseResponseEntity);
    }

    @Test
    public void testGetLocation() throws Exception {
        WeatherLogResponse weatherLogResponse = new WeatherLogResponse();
        WeatherLogSysResponse sysResponse = new WeatherLogSysResponse();
        sysResponse.setCountry("country");
        weatherLogResponse.setWeatherLogSysResponse(sysResponse);
        weatherLogResponse.setName("any");

        String actual = Whitebox.invokeMethod(sut, "getLocation", weatherLogResponse);

        Assert.assertTrue(actual.contains("any, country"));
    }

    @Test
    public void testCheckActualWeather_EmptyDetailResponses() throws Exception {
        List<WeatherLogDetailResponse> weatherLogDetailResponses = new ArrayList<>();
        WeatherLog weatherLog = new WeatherLog();

        Whitebox.invokeMethod(sut, "checkActualWeather", weatherLog, weatherLogDetailResponses);
    }

    @Test
    public void testCheckActualWeather_NotEmptyDetailResponses() throws Exception {
        List<WeatherLogDetailResponse> weatherLogDetailResponses = new ArrayList<>();
        WeatherLogDetailResponse weatherLogDetailResponse = new WeatherLogDetailResponse();
        weatherLogDetailResponse.setDescription("rainy");

        WeatherLogDetailResponse weatherLogDetailResponse1 = new WeatherLogDetailResponse();
        weatherLogDetailResponse.setDescription("stormy");

        weatherLogDetailResponses
            .addAll(Arrays.asList(weatherLogDetailResponse, weatherLogDetailResponse1));

        WeatherLog weatherLog = new WeatherLog();

        Whitebox.invokeMethod(sut, "checkActualWeather", weatherLog, weatherLogDetailResponses);
    }

    @Test
    public void testGenerateResponseId() throws Exception {
        String actual = Whitebox.invokeMethod(sut, "generateResponseId");
        Assert.assertNotNull(actual);
    }

    @Test
    public void testBuildWeatherLog() throws Exception {
        WeatherLogResponse weatherLogResponse = new WeatherLogResponse();
        WeatherLogSysResponse sysResponse = new WeatherLogSysResponse();
        sysResponse.setCountry("New Zealand");
        weatherLogResponse.setWeatherLogSysResponse(sysResponse);
        WeatherLogMainResponse weatherLogMainResponse = new WeatherLogMainResponse();
        weatherLogMainResponse.setTemp(1.5);
        weatherLogResponse.setWeatherLogMainResponse(weatherLogMainResponse);

        WeatherLog actual = Whitebox.invokeMethod(sut, "build", weatherLogResponse);

        Assert.assertTrue(actual.getTemperature().contains("1.5"));
        Assert.assertTrue(actual.getLocation().contains("New Zealand"));
    }

    @Test
    public void testGetWeatherLogResponseList() throws Exception {

        List<WeatherLogResponse> actual = Whitebox.invokeMethod(sut, "getWeatherLogResponseList");

        Assert.assertNull(actual);
    }

    @Test
    public void testBuildAndSave_EmptyResponseList() throws Exception {
        List<WeatherLogShortInfoResponse> infoResponses = new ArrayList<>();
        List<WeatherLogResponse> weatherLogResponseList = new ArrayList<>();

        Whitebox.invokeMethod(sut, "buildAndSave", infoResponses, Boolean.FALSE,
            weatherLogResponseList);
    }


    @Test
    public void testBuildAndSave_NotEmptyResponseList() throws Exception {
        List<WeatherLogShortInfoResponse> infoResponses = new ArrayList<>();
        List<WeatherLogResponse> weatherLogResponseList = new ArrayList<>();
        WeatherLogResponse weatherLogResponse = new WeatherLogResponse();
        WeatherLogSysResponse sysResponse = new WeatherLogSysResponse();
        sysResponse.setCountry("New Zealand");
        weatherLogResponse.setWeatherLogSysResponse(sysResponse);
        WeatherLogMainResponse weatherLogMainResponse = new WeatherLogMainResponse();
        weatherLogMainResponse.setTemp(1.5);
        weatherLogResponse.setWeatherLogMainResponse(weatherLogMainResponse);
        weatherLogResponseList.add(weatherLogResponse);

        // execute build
        Whitebox.invokeMethod(sut, "buildAndSave", infoResponses, Boolean.FALSE,
            weatherLogResponseList);

        // execute build and save
        Whitebox
            .invokeMethod(sut, "buildAndSave", infoResponses, Boolean.TRUE, weatherLogResponseList);
    }

    @Test
    public void testGetWeatherData() {

        WeatherLogShortInfoListResponse actual = sut.getWeatherData();

        Assert.assertNotNull(actual.getList());
    }


    @Test
    public void testSave() {

        WeatherLogShortInfoListResponse actual = sut.save();

        Assert.assertNotNull(actual.getList());
    }
}
