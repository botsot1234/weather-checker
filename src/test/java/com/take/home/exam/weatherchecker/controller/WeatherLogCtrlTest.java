package com.take.home.exam.weatherchecker.controller;

import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoListResponse;
import com.take.home.exam.weatherchecker.dto.WeatherLogShortInfoResponse;
import com.take.home.exam.weatherchecker.service.WeatherLogService;
import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherLogCtrl.class)
@Slf4j
@AutoConfigureMockMvc
public class WeatherLogCtrlTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WeatherLogService weatherLogService;

    private static final String ANY = "ANY";

    @Test
    public void testCheck() throws Exception {
        WeatherLogShortInfoResponse weatherLogShortInfoResponse = new WeatherLogShortInfoResponse(
            ANY, ANY, ANY);
        WeatherLogShortInfoListResponse weatherLogShortInfoListResponse = new WeatherLogShortInfoListResponse();
        weatherLogShortInfoListResponse.setList(Arrays.asList(weatherLogShortInfoResponse));
        Mockito.when(weatherLogService.getWeatherData())
            .thenReturn(weatherLogShortInfoListResponse);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/check").contentType(
            MediaType.APPLICATION_JSON_VALUE)).andDo(MockMvcResultHandlers.print()).andExpect(
            MockMvcResultMatchers.content().contentTypeCompatibleWith(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

        String res = result.getResponse().getContentAsString();

        Assert.assertTrue(res.contains(ANY));
    }


    @Test
    public void testSave() throws Exception {
        WeatherLogShortInfoResponse weatherLogShortInfoResponse = new WeatherLogShortInfoResponse(
            ANY, ANY, ANY);
        WeatherLogShortInfoListResponse weatherLogShortInfoListResponse = new WeatherLogShortInfoListResponse();
        weatherLogShortInfoListResponse.setList(Arrays.asList(weatherLogShortInfoResponse));
        Mockito.when(weatherLogService.save()).thenReturn(weatherLogShortInfoListResponse);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/store").contentType(
            MediaType.APPLICATION_JSON_VALUE)).andDo(MockMvcResultHandlers.print()).andExpect(
            MockMvcResultMatchers.content().contentTypeCompatibleWith(
                MediaType.APPLICATION_JSON_VALUE)).andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

        String responseStr = result.getResponse().getContentAsString();

        Assert.assertTrue(responseStr.contains(ANY));
    }
}
